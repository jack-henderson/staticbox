/*
	Minimal reporter which logs only failed tests.
*/

const mocha = require('mocha');


const { EVENT_TEST_FAIL, EVENT_TEST_PASS, EVENT_RUN_END,  } = mocha.Runner.constants;

module.exports = function(runner) {
	mocha.reporters.Base.call(this, runner);

	let passed = 0;
	let failed = [];

	const title = test => {
		const segments = [];
		while(test) {
			segments.push(test.title);
			test = test.parent
		}
		return segments.filter(s => s.length).reverse().join(' / ');
	}

	const stack = err => {
		const ln = err.stack.split('\n')[1];
		const start = ln.indexOf('(') + 1;
		const end = ln.indexOf(')');
		return ln.substring(start, end);
	}

	runner.on(EVENT_TEST_FAIL, (test, err) => {
		failed.push(` ✖ ${title(test)}\n   ${stack(err)}\n   ${err.message}`);
	});

	runner.on(EVENT_TEST_PASS, () => ++passed);

	runner.on(EVENT_RUN_END, () => {
		const numTests = passed + failed.length;

		console.log(` ✔ ${passed}/${numTests} tests passed.`);
		failed.length && console.log(failed.join('\n\n'));
	})
}
