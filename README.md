<center>

**StaticBox**
=============

Generate plain TypeScript definitions from [TypeBox](https://github.com/sinclairzx81/typebox#readme) schema.

[![npm](https://img.shields.io/npm/v/staticbox)](https://www.npmjs.com/package/staticbox)
[![pipeline status](https://gitlab.com/jack-henderson/staticbox/badges/develop/pipeline.svg)](https://gitlab.com/jack-henderson/staticbox/-/commits/develop)
[![coverage report](https://gitlab.com/jack-henderson/staticbox/badges/develop/coverage.svg)](https://gitlab.com/jack-henderson/staticbox/-/commits/develop) 

</center>

----

